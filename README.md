**The development of RootStemExtractor/Interekt has been moved to another GitLab repository**:
[https://forgemia.inra.fr/felix.hartmann/interekt](https://forgemia.inra.fr/felix.hartmann/interekt)	

**This repository is no longer active.**


![Screenshot](https://github.com/hchauvet/RootStemExtractor/raw/master/img/Screenshot1.png "screenshot")
